package dev.vriska.jpubgrub;

import dev.vriska.jpubgrub.versions.VersionConstraint;

public record PackageRef(String name, boolean isRoot) {
    public PackageRange withConstraint(VersionConstraint constraint) {
        return new PackageRange(this, constraint);
    }
}
