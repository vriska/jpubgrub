package dev.vriska.jpubgrub;

/**
 * The incompatibility indicates that the package has no versions that match the given constraint.
 */
public class NoVersionsIncompatibilityCause extends IncompatibilityCause {

}
