package dev.vriska.jpubgrub;

import dev.vriska.jpubgrub.versions.VersionConstraint;

import java.util.Objects;

/**
 * A statement about a package which is true or false for a given selection of package versions.
 * <p>
 * See <a href="https://github.com/dart-lang/pub/tree/master/doc/solver.md#term">solver.md</a>.
 */
public class Term {
    private final PackageRange packageRange;
    private final boolean isPositive;

    public Term(PackageRange packageRange, boolean isPositive) {
        this.packageRange = packageRange;
        this.isPositive = isPositive;
    }

    /**
     * A copy of this term with the opposite {@code isPositive} value.
     */
    public Term inverse() {
        return new Term(packageRange(), !isPositive());
    }

    public VersionConstraint constraint() {
        return packageRange().constraint();
    }

    /**
     * Returns whether this term satisfies {@code other}.
     * <p>
     * That is, whether this term being true mans that {@code other} must also be true.
     */
    public boolean satisfies(Term other) {
        return packageRange().name().equals(other.packageRange().name()) && relation(other) == SetRelation.SUBSET;
    }

    /**
     * Returns the relationship between the package versions allowed by this term and by {@code other}.
     *
     * @throws IllegalArgumentException If {@code other} doesn't refer to a package with the same name as this term
     */
    public SetRelation relation(Term other) {
        if (!packageRange().name().equals(other.packageRange().name())) {
            throw new IllegalArgumentException(packageRange().name() + " != " + other.packageRange().name());
        }

        var otherConstraint = other.constraint();
        if (other.isPositive()) {
            if (isPositive()) {
                // foo ^1.5.0 is a subset of foo ^1.0.0
                if (otherConstraint.allowsAll(constraint())) return SetRelation.SUBSET;

                // foo ^2.0.0 is disjoint with foo ^1.0.0
                if (!constraint().allowsAny(otherConstraint)) return SetRelation.DISJOINT;

                // foo >=1.5.0 <3.0.0 overlaps foo ^1.0.0
                return SetRelation.OVERLAPPING;
            } else {
                // not foo ^1.0.0 is disjoint with foo ^1.5.0
                if (constraint().allowsAll(otherConstraint)) return SetRelation.DISJOINT;

                // not foo ^1.5.0 overlaps foo ^1.0.0
                // not foo ^2.0.0 is a superset of foo ^1.5.0
                return SetRelation.OVERLAPPING;
            }
        } else {
            if (isPositive()) {
                // foo ^2.0.0 is a subset of not foo ^1.0.0
                if (!otherConstraint.allowsAny(constraint())) return SetRelation.SUBSET;

                // foo ^1.5.0 is disjoint with not foo ^1.0.0
                if (otherConstraint.allowsAll(constraint())) return SetRelation.DISJOINT;

                // foo ^1.0.0 overlaps not foo ^1.5.0
                return SetRelation.OVERLAPPING;
            } else {
                // not foo ^1.0.0 is a subset of not foo ^1.5.0
                if (constraint().allowsAll(otherConstraint)) return SetRelation.SUBSET;

                // not foo ^2.0.0 overlaps not foo ^1.0.0
                // not foo ^1.5.0 is a superset of not foo ^1.0.0
                return SetRelation.OVERLAPPING;
            }
        }
    }

    /**
     * Returns a Term that represents the packages allowed by both this term and {@code other}.
     * <p>
     * If there is no such single term, for example because this term is incompatible with {@code other}, returns null.
     *
     * @throws IllegalArgumentException If {@code other} doesn't refer to a package with the same name as this term
     */
    public Term intersect(Term other) {
        if (!packageRange().name().equals(other.packageRange().name())) {
            throw new IllegalArgumentException(packageRange().name() + " != " + other.packageRange().name());
        }

        if (isPositive() != other.isPositive()) {
            // foo ^1.0.0 ∩ not foo ^1.5.0 → foo >=1.0.0 <1.5.0
            var positive = isPositive() ? this : other;
            var negative = isPositive() ? other : this;
            return nonEmptyTerm(positive.constraint().difference(negative.constraint()), true);
        } else if (isPositive()) {
            // foo ^1.0.0 ∩ foo >=1.5.0 <3.0.0 → foo ^1.5.0
            return nonEmptyTerm(constraint().intersect(other.constraint()), true);
        } else {
            // not foo ^1.0.0 ∩ not foo >=1.5.0 <3.0.0 → not foo >=1.0.0 <3.0.0
            return nonEmptyTerm(constraint().union(other.constraint()), false);
        }
    }

    /**
     * Returns a Term that represents the packages allowed by this term and not by {@code other}.
     * <p>
     * If there is no such single term, for example because all packages allowed by this term are allowed by
     * {@code other}, returns null.
     *
     * @throws IllegalArgumentException If {@code other} doesn't refer to a package with the same name as this term
     */
    public Term difference(Term other) {
        return intersect(other.inverse());
    }

    Term nonEmptyTerm(VersionConstraint constraint, boolean isPositive) {
        if (constraint.isEmpty()) {
            return null;
        } else {
            return new Term(packageRange().ref().withConstraint(constraint), isPositive());
        }
    }

    @Override
    public String toString() {
        return (isPositive() ? "" : "not ") + packageRange();
    }

    /**
     * The range of package versions referred to by this term.
     */
    public PackageRange packageRange() {
        return packageRange;
    }

    /**
     * Whether the term is positive or not.
     * <p>
     * A positive constraint is true when a package version that matches {@code packageRange} is selected; a negative
     * constraint is true when no package versions that match {@code packageRange} are selected.
     */
    public boolean isPositive() {
        return isPositive;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (Term) obj;
        return Objects.equals(this.packageRange, that.packageRange) && this.isPositive == that.isPositive;
    }

    @Override
    public int hashCode() {
        return Objects.hash(packageRange, isPositive);
    }

}
