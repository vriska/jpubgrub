package dev.vriska.jpubgrub;

import java.util.List;
import java.util.Map;

public record Result(List<PackageId> packages, Map<String, String> selectedVersions,
                     Map<String, List<String>> availableVersions, int attemptedSolutions) {
}
