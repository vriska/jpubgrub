package dev.vriska.jpubgrub;

public enum SetRelation {
    SUBSET, DISJOINT, OVERLAPPING,
}
