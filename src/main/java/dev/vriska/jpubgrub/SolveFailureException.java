package dev.vriska.jpubgrub;

import java.util.*;

/**
 * An exception indicating that version solving failed.
 */
public class SolveFailureException extends RuntimeException {
    /**
     * The root incompatibility.
     * <p>
     * This will always indicate that the root package is unselectable. That is, it will have one term, which will be
     * the root package.
     */
    public final Incompatibility incompatibility;

    public final String suggestions;

    /**
     * @throws IllegalArgumentException if incompatibility does not have at most one term, for the root package
     */
    public SolveFailureException(Incompatibility incompatibility, String suggestions) {
        super(constructMessage(incompatibility, suggestions));

        this.incompatibility = incompatibility;
        this.suggestions = suggestions;
    }

    private static String constructMessage(Incompatibility incompatibility, String suggestions) {
        if (!incompatibility.terms.isEmpty() && !incompatibility.terms.get(0).packageRange().isRoot()) {
            throw new IllegalArgumentException(incompatibility + " not for the root package");
        }

        var buffer = new StringBuilder();
        buffer.append(new Writer(incompatibility).write());
        if (suggestions != null) {
            buffer.append('\n');
            buffer.append(suggestions);
        }
        return buffer.toString();
    }

    /**
     * A class that writes a human-readable description of the cause of a SolveFailureException.
     * <p>
     * See <a href="https://github.com/dart-lang/pub/tree/master/doc/solver.md#error-reporting">solver.md</a> for
     * details on how this algorithm works.
     */
    private static class Writer {
        /**
         * The root incompatibility.
         */
        private final Incompatibility root;

        /**
         * The number of times each {@link Incompatibility} appears in {@code root}'s derivation tree.
         * <p>
         * When an Incompatibility is used in multiple derivations, we need to give it a number so we can refer back to
         * it later on.
         */
        private final Map<Incompatibility, Integer> derivations = new HashMap<>();

        private record Line(String message, Integer number) {

        }

        /**
         * The lines in the proof.
         * <p>
         * Each line is a message/number pair. The message describes a single incompatibility, and why its terms are
         * incompatible. The number is optional and indicates the explicit number that should be associated with the
         * line so it can be referred to later on.
         */
        private final List<Line> lines = new ArrayList<>();

        /**
         * A map from incompatibilities to the line numbers that were written for those incompatibilities.
         */
        private final Map<Incompatibility, Integer> lineNumbers = new LinkedHashMap<>();

        public Writer(Incompatibility root) {
            this.root = root;

            countDerivations(root);
        }

        /**
         * Populates {@code derivations} for {@code incompatibility} and its transitive causes.
         */
        private void countDerivations(Incompatibility incompatibility) {
            derivations.compute(incompatibility, (k, v) -> {
                if (v != null) {
                    return v + 1;
                } else {
                    if (k.cause instanceof ConflictCause cause) {
                        countDerivations(cause.conflict);
                        countDerivations(cause.other);
                    }
                    return 1;
                }
            });
        }

        public String write() {
            var buffer = new StringBuilder();

            if (root.cause instanceof ConflictCause) {
                visit(root, false);
            } else {
                write(root, "Because " + root + ", version solving failed.");
            }

            var padding = 0;
            if (!lineNumbers.isEmpty()) {
                for (var value : lineNumbers.values()) {
                    padding = value.toString().length() + 3;
                }
            }

            var lastWasEmpty = false;
            for (var line : lines) {
                var message = line.message();
                if (message.isEmpty()) {
                    if (!lastWasEmpty) buffer.append('\n');
                    lastWasEmpty = true;
                    continue;
                } else {
                    lastWasEmpty = false;
                }

                if (line.number() != null) {
                    message = String.format("%-" + padding + "s", "(" + message + ")");
                } else {
                    message = " ".repeat(padding) + message;
                }

                buffer.append(message).append('\n');
            }

            return buffer.toString();
        }

        /**
         * Writes {@code message} to {@code lines}.
         * <p>
         * The {@code message} should describe {@code incompatibility} and how it was derived (if applicable). If
         * {@code numbered} is true, this will associate a line number with {@code incompatibility} and {@code message}
         * so that the message can be easily referred to later.
         */
        private void write(Incompatibility incompatibility, String message, boolean numbered) {
            if (numbered) {
                var number = lineNumbers.size() + 1;
                lineNumbers.put(incompatibility, number);
                lines.add(new Line(message, number));
            } else {
                lines.add(new Line(message, null));
            }
        }

        private void write(Incompatibility incompatibility, String message) {
            write(incompatibility, message, false);
        }

        /**
         * Writes a proof of {@code incompatibility} to {@code lines}.
         * <p>
         * If {@code conclusion} is true, {@code incompatibility} represents the last of a linear series of derivations.
         * It should be phrased accordingly and given a line number.
         */
        private void visit(Incompatibility incompatibility, boolean conclusion) {
            var numbered = conclusion || derivations.get(incompatibility) > 1;
            var conjunction = conclusion || incompatibility == root ? "So," : "And";
            var incompatibilityString = incompatibility.toString();
        }
    }
}
