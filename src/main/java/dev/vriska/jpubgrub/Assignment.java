package dev.vriska.jpubgrub;

/**
 * A term in a {@link PartialSolution} that tracks some additional metadata.
 */
public class Assignment extends Term {
    /**
     * The number of decisions at or before this in the {@link PartialSolution} that contains it.
     */
    public final int decisionLevel;

    /**
     * The index of this assignment in the {@link PartialSolution} that contains it.
     */
    public final int index;

    /**
     * The incompatibility that caused this assignment to be derived, or null if the assignment isn't a derivation.
     */
    public final Incompatibility cause;

    /**
     * Whether this assignment is a decision, as opposed to a derivation.
     */
    public boolean isDecision() {
        return cause != null;
    }

    /**
     * Creates a decision: a speculative assignment of a single package version.
     */
    public Assignment(PackageId packageId, int decisionLevel, int index) {
        super(packageId.toRange(), true);
        this.cause = null;
        this.decisionLevel = decisionLevel;
        this.index = index;
    }

    /***
     * Creates a derivation: an assignment that's automatically propagated from incompatibilities.
     */
    public Assignment(PackageRange packageRange, boolean isPositive, Incompatibility cause, int decisionLevel, int index) {
        super(packageRange, isPositive);
        this.cause = cause;
        this.decisionLevel = decisionLevel;
        this.index = index;
    }
}
