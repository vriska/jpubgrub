package dev.vriska.jpubgrub.versions;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.BooleanSupplier;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

/**
 * A version constraint representing a union of multiple disjoint version ranges.
 * <p>
 * An instance of this will only be created if the version can't be represented as a non-compound value.
 *
 * @param ranges the constraints that compose this union, which must be sorted, disjoint, and non-adjacent
 */
public record VersionUnion(List<VersionRange> ranges) implements VersionConstraint {
    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean isAny() {
        return false;
    }

    @Override
    public boolean allows(Version version) {
        return ranges.stream().anyMatch(constraint -> constraint.allows(version));
    }

    // this is super gnarly bc it was literally translated from dart iterators and trying to simplify it gave me a
    // headache
    @Override
    public boolean allowsAll(VersionConstraint other) {
        var ourRanges = ranges.iterator();
        var theirRanges = rangesFor(other).iterator();

        if (!ourRanges.hasNext() || !theirRanges.hasNext()) {
            return !theirRanges.hasNext();
        }

        VersionRange ours = ourRanges.next();
        VersionRange theirs = theirRanges.next();
        boolean ourRangesMoved = true;
        boolean theirRangesMoved = true;

        while (ourRangesMoved && theirRangesMoved) {
            if (ours.allowsAll(theirs)) {
                theirRangesMoved = theirRanges.hasNext();
                if (theirRangesMoved) theirs = theirRanges.next();
            } else {
                ourRangesMoved = ourRanges.hasNext();
                if (ourRangesMoved) ours = ourRanges.next();
            }
        }

        return !theirRangesMoved;
    }

    @Override
    public boolean allowsAny(VersionConstraint other) {
        var ourRanges = ranges.iterator();
        var theirRanges = rangesFor(other).iterator();

        if (!ourRanges.hasNext() || !theirRanges.hasNext()) {
            return false;
        }

        VersionRange ours = ourRanges.next();
        VersionRange theirs = theirRanges.next();
        boolean ourRangesMoved = true;
        boolean theirRangesMoved = true;

        while (ourRangesMoved && theirRangesMoved) {
            if (ours.allowsAny(theirs)) return true;

            if (theirs.allowsHigher(ours)) {
                ourRangesMoved = ourRanges.hasNext();
                if (ourRangesMoved) ours = ourRanges.next();
            } else {
                theirRangesMoved = theirRanges.hasNext();
                if (theirRangesMoved) theirs = theirRanges.next();
            }
        }

        return false;
    }

    @Override
    public @NotNull VersionConstraint intersect(VersionConstraint other) {
        var ourRanges = ranges.iterator();
        var theirRanges = rangesFor(other).iterator();

        if (!ourRanges.hasNext() || !theirRanges.hasNext()) {
            return VersionConstraint.EMPTY;
        }

        var newRanges = new ArrayList<VersionRange>();

        VersionRange ours = ourRanges.next();
        VersionRange theirs = theirRanges.next();
        boolean ourRangesMoved = true;
        boolean theirRangesMoved = true;

        while (ourRangesMoved && theirRangesMoved) {
            var intersection = ours.intersect(theirs);

            if (!intersection.isEmpty()) newRanges.add((VersionRange) intersection);

            if (theirs.allowsHigher(ours)) {
                ourRangesMoved = ourRanges.hasNext();
                if (ourRangesMoved) ours = ourRanges.next();
            } else {
                theirRangesMoved = theirRanges.hasNext();
                if (theirRangesMoved) theirs = theirRanges.next();
            }
        }

        if (newRanges.isEmpty()) return VersionConstraint.EMPTY;
        if (newRanges.size() == 1) return newRanges.get(0);

        return new VersionUnion(newRanges);
    }

    @Override
    public @NotNull VersionConstraint union(VersionConstraint other) {
        return VersionConstraint.unionOf(Stream.of(this, other));
    }

    @Override
    public @NotNull VersionConstraint difference(VersionConstraint other) {
        var ourRanges = ranges.iterator();
        var theirRanges = rangesFor(other).iterator();

        final VersionRange[] current = {ourRanges.next()};
        final VersionRange[] theirs = {theirRanges.next()};

        var newRanges = new ArrayList<VersionRange>();

        BooleanSupplier theirNextRange = () -> {
            if (theirRanges.hasNext()) {
                theirs[0] = theirRanges.next();
                return true;
            } else {
                newRanges.add(current[0]);
                while (ourRanges.hasNext()) {
                    newRanges.add(ourRanges.next());
                }
                return false;
            }
        };

        BooleanSupplier ourNextRangeWithoutCurrent = () -> {
            if (!ourRanges.hasNext()) {
                return false;
            } else {
                current[0] = ourRanges.next();
                return true;
            }
        };

        BooleanSupplier ourNextRange = () -> {
            newRanges.add(current[0]);
            return ourNextRangeWithoutCurrent.getAsBoolean();
        };

        while (true) {
            if (theirs[0].strictlyLower(current[0])) {
                if (!theirNextRange.getAsBoolean()) break;
                continue;
            }

            if (theirs[0].strictlyHigher(current[0])) {
                if (!ourNextRange.getAsBoolean()) break;
                continue;
            }

            var difference = current[0].difference(theirs[0]);
            if (difference instanceof VersionUnion versionUnion) {
                if (versionUnion.ranges.size() != 2) throw new RuntimeException("impossible difference result");
                newRanges.add(versionUnion.ranges.get(0));
                current[0] = versionUnion.ranges.get(1);

                if (!theirNextRange.getAsBoolean()) break;
            } else if (difference.isEmpty()) {
                if (!ourNextRangeWithoutCurrent.getAsBoolean()) break;
            } else {
                current[0] = (VersionRange) difference;

                if (current[0].allowsHigher(theirs[0])) {
                    if (!theirNextRange.getAsBoolean()) break;
                } else {
                    if (!ourNextRange.getAsBoolean()) break;
                }
            }
        }

        if (newRanges.isEmpty()) return VersionConstraint.EMPTY;
        if (newRanges.size() == 1) return newRanges.get(0);
        return new VersionUnion(newRanges);
    }

    private static List<VersionRange> rangesFor(VersionConstraint constraint) {
        if (constraint.isEmpty()) {
            return Collections.emptyList();
        } else if (constraint instanceof VersionRange versionRange) {
            return List.of(versionRange);
        } else if (constraint instanceof VersionUnion versionUnion) {
            return versionUnion.ranges();
        }
        throw new IllegalArgumentException();
    }
}
