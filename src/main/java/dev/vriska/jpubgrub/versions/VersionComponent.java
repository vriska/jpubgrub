package dev.vriska.jpubgrub.versions;

import java.util.Arrays;

sealed class VersionComponent {
    public static final VersionComponent NULL = new NullVersionComponent();
    private final int[] codepoints;

    public VersionComponent(int[] codepoints) {
        this.codepoints = codepoints;
    }

    public int[] codepoints() {
        return codepoints;
    }

    public int compareTo(VersionComponent that) {
        if (that == VersionComponent.NULL) return 1;
        int[] a = this.codepoints();
        int[] b = that.codepoints();

        for (int i = 0; i < Math.min(a.length, b.length); i++) {
            int c1 = a[i];
            int c2 = b[i];
            if (c1 != c2) return c1 - c2;
        }

        return a.length - b.length;
    }

    @Override
    public String toString() {
        return new String(codepoints, 0, codepoints.length);
    }

    private static final class NullVersionComponent extends VersionComponent {
        public NullVersionComponent() {
            super(new int[0]);
        }

        @Override
        public int compareTo(VersionComponent other) {
            return other == NULL ? 0 : -other.compareTo(this);
        }
    }

    static final class NumericVersionComponent extends VersionComponent {
        public NumericVersionComponent(int[] codepoints) {
            super(codepoints);
        }

        @Override
        public int compareTo(VersionComponent that) {
            if (that == NULL) return 1;
            if (that instanceof NumericVersionComponent) {
                int[] a = removeLeadingZeroes(this.codepoints());
                int[] b = removeLeadingZeroes(that.codepoints());
                if (a.length != b.length) return a.length - b.length;
                for (int i = 0; i < a.length; i++) {
                    int ad = a[i];
                    int bd = b[i];
                    if (ad != bd) return ad - bd;
                }
                return 0;
            }
            return super.compareTo(that);
        }

        private int[] removeLeadingZeroes(int[] a) {
            if (a.length == 1) return a;
            int i = 0;
            while (i < a.length && a[i] == '0') {
                i++;
            }
            return Arrays.copyOfRange(a, i, a.length);
        }

    }

    static final class SemVerPrereleaseVersionComponent extends VersionComponent {
        public SemVerPrereleaseVersionComponent(int[] codepoints) {
            super(codepoints);
        }

        @Override
        public int compareTo(VersionComponent that) {
            if (that == NULL) return -1; // opposite order
            return super.compareTo(that);
        }

    }
}
