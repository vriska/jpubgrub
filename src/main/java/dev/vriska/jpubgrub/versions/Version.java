package dev.vriska.jpubgrub.versions;

import java.util.*;

/**
 * Implements FlexVer, a SemVer-compatible intuitive comparator for free-form versioning strings as seen in the wild.
 * It's designed to sort versions like people do, rather than attempting to force conformance to a rigid and limited
 * standard. As such, it imposes no restrictions. Comparing two versions with differing formats will likely produce
 * nonsensical results (garbage in, garbage out), but best effort is made to correct for basic structural changes, and
 * versions of differing length will be parsed in a logical fashion.
 */
public final class Version extends VersionRange {
    private final String str;
    private final List<VersionComponent> components;

    public Version(String str) {
        super(null, null, true, true);
        this.str = str;
        this.components = decompose(str);
    }

    @Override
    public Version max() {
        return this;
    }

    @Override
    public Version min() {
        return this;
    }

    @Override
    public int compareTo(VersionRange range) {
        if (range instanceof Version o) {
            List<VersionComponent> ad = this.components;
            List<VersionComponent> bd = o.components;
            for (int i = 0; i < Math.max(ad.size(), bd.size()); i++) {
                int c = get(ad, i).compareTo(get(bd, i));
                if (c != 0) return c;
            }
            return 0;
        } else {
            return super.compareTo(range);
        }
    }

    /*
     * Break apart a string into intuitive version components, by splitting it where a run of
     * characters changes from numeric to non-numeric.
     */
    private static List<VersionComponent> decompose(String str) {
        if (str.isEmpty()) return Collections.emptyList();
        boolean lastWasNumber = isAsciiDigit(str.codePointAt(0));
        int totalCodepoints = str.codePointCount(0, str.length());
        int[] accum = new int[totalCodepoints];
        List<VersionComponent> out = new ArrayList<>();
        int j = 0;
        for (int i = 0; i < str.length(); i++) {
            int cp = str.codePointAt(i);
            if (Character.charCount(cp) == 2) i++;
            if (cp == '+') break; // remove appendices
            boolean number = isAsciiDigit(cp);
            if (number != lastWasNumber || (cp == '-' && j > 0 && accum[0] != '-')) {
                out.add(createComponent(lastWasNumber, accum, j));
                j = 0;
                lastWasNumber = number;
            }
            accum[j] = cp;
            j++;
        }
        out.add(createComponent(lastWasNumber, accum, j));
        return out;
    }

    private static boolean isAsciiDigit(int cp) {
        return cp >= '0' && cp <= '9';
    }

    private static VersionComponent createComponent(boolean number, int[] s, int j) {
        s = Arrays.copyOfRange(s, 0, j);
        if (number) {
            return new VersionComponent.NumericVersionComponent(s);
        } else if (s.length > 1 && s[0] == '-') {
            return new VersionComponent.SemVerPrereleaseVersionComponent(s);
        } else {
            return new VersionComponent(s);
        }
    }

    private static VersionComponent get(List<VersionComponent> li, int i) {
        return i >= li.size() ? VersionComponent.NULL : li.get(i);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Version version) {
            return compareTo(version) == 0;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(components);
    }

    @Override
    public String toString() {
        return str;
    }

}
