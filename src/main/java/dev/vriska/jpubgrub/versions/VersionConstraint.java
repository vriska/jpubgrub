package dev.vriska.jpubgrub.versions;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A VersionConstraint is a predicate that can determine whether a given version is valid or not.
 * <p>
 * For example, a ">= 2.0.0" constraint allows any version that is "2.0.0" or greater. Version objects themselves
 * implement this to match a specific version.
 */
public sealed interface VersionConstraint permits EmptyVersion, VersionRange, VersionUnion {
    VersionConstraint ANY = new VersionRange(null, null, false, false);
    VersionConstraint EMPTY = new EmptyVersion();

    /**
     * Returns {@code true} if this constraint allows no versions.
     */
    boolean isEmpty();

    /**
     * Returns {@code true} if this constraint allows all versions.
     */
    boolean isAny();

    /**
     * Returns {@code true} if this constraint allows {@code version}.
     */
    boolean allows(Version version);

    /**
     * Returns {@code true} if this constraint allows all the versions that {@code other} allows.
     */
    boolean allowsAll(VersionConstraint other);

    /**
     * Returns {@code true} if this constraint allows any of the versions that {@code other} allows.
     */
    boolean allowsAny(VersionConstraint other);

    /**
     * Returns a VersionConstraint that only allows {@link Version}s allowed by both this and {@code other}.
     */
    @NotNull VersionConstraint intersect(VersionConstraint other);

    /**
     * Returns a VersionConstraint that only allows {@link Version}s allowed by either this or {@code other}.
     */
    @NotNull VersionConstraint union(VersionConstraint other);

    /**
     * Returns a VersionConstraint that only allows {@link Version}s allowed by this but not {@code other}.
     */
    @NotNull VersionConstraint difference(VersionConstraint other);

    /**
     * Creates a new version constraint that is the union of {@code constraints}.
     * <p>
     * It allows any versions that any of those constraints allows. If {@code constraints} is empty, this returns a
     * constraint that allows no versions.
     */
    static VersionConstraint unionOf(Stream<VersionConstraint> constraints) {
        var flattened = constraints.flatMap(constraint -> {
            if (constraint.isEmpty()) {
                return Stream.empty();
            } else if (constraint instanceof VersionRange versionRange) {
                return Stream.of(versionRange);
            } else if (constraint instanceof VersionUnion versionUnion) {
                return versionUnion.ranges().stream();
            }
            throw new IllegalArgumentException();
        }).collect(Collectors.toCollection(ArrayList::new));

        if (flattened.isEmpty()) return EMPTY;
        if (flattened.stream().anyMatch(VersionConstraint::isAny)) return ANY;

        Collections.sort(flattened);

        var merged = new ArrayList<VersionRange>();
        for (var constraint : flattened) {
            if (merged.isEmpty()) {
                merged.add(constraint);
            } else {
                var last = merged.get(merged.size() - 1);
                if (!last.allowsAny(constraint) && !last.areAdjacent(constraint)) {
                    merged.add(constraint);
                } else {
                    merged.set(merged.size() - 1, (VersionRange) last.union(constraint));
                }
            }
        }

        if (merged.size() == 1) return merged.get(0);

        return new VersionUnion(merged);
    }
}
