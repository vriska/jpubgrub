package dev.vriska.jpubgrub.versions;

import org.jetbrains.annotations.NotNull;

final class EmptyVersion implements VersionConstraint {
    @Override
    public boolean isEmpty() {
        return true;
    }

    @Override
    public boolean isAny() {
        return false;
    }

    @Override
    public boolean allows(Version version) {
        return false;
    }

    @Override
    public boolean allowsAll(VersionConstraint other) {
        return other.isEmpty();
    }

    @Override
    public boolean allowsAny(VersionConstraint other) {
        return false;
    }

    @Override
    public @NotNull VersionConstraint intersect(VersionConstraint other) {
        return this;
    }

    @Override
    public @NotNull VersionConstraint union(VersionConstraint other) {
        return other;
    }

    @Override
    public @NotNull VersionConstraint difference(VersionConstraint other) {
        return this;
    }

    @Override
    public String toString() {
        return "<empty>";
    }
}
