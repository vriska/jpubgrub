package dev.vriska.jpubgrub.versions;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * Constrains versions to a fall within a given range.
 * <p>
 * If there is a minimum, then this only allows versions that are at that minimum or greater. If there is a maximum,
 * then only versions less than that are allowed. In other words, this allows {@code >= min, < max}.
 * <p>
 * Version ranges are ordered first by their lower bounds, then by their upper bounds. For example,
 * {@code >=1.0.0 <2.0.0} is before {@code >=1.5.0 <2.0.0} is before {@code >=1.5.0 <3.0.0}.
 */
public sealed class VersionRange implements Comparable<VersionRange>, VersionConstraint permits Version {
    private final Version min;
    private final Version max;
    private final boolean includeMin;
    private final boolean includeMax;

    /**
     * @param min        the minimum end of the range, which may be null in which case the range has no minimum end
     * @param max        the maximum end of the range, which may be null in which case the range has no maximum end
     * @param includeMin if true, then min is allowed by the range
     * @param includeMax if true, then max is allowed by the range
     */
    public VersionRange(Version min, Version max, boolean includeMin, boolean includeMax) {
        this.min = min;
        this.max = max;
        this.includeMin = includeMin;
        this.includeMax = includeMax;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean isAny() {
        return min() == null && max() == null;
    }

    @Override
    public boolean allows(Version other) {
        if (min() != null) {
            if (other.compareTo(min()) < 0) return false;
            if (!includeMin() && other.compareTo(min()) == 0) return false;
        }

        if (max() != null) {
            if (other.compareTo(max()) > 0) return false;
            if (!includeMax() && other.compareTo(max()) == 0) return false;
        }

        return true;
    }

    @Override
    public boolean allowsAll(VersionConstraint other) {
        if (other.isEmpty()) {
            return true;
        } else if (other instanceof Version version) {
            return allows(version);
        } else if (other instanceof VersionRange versionRange) {
            return !versionRange.allowsLower(this) && !versionRange.allowsHigher(this);
        } else if (other instanceof VersionUnion versionUnion) {
            return versionUnion.ranges().stream().allMatch(this::allowsAll);
        }
        throw new IllegalArgumentException();
    }

    @Override
    public boolean allowsAny(VersionConstraint other) {
        if (other.isEmpty()) {
            return false;
        } else if (other instanceof Version version) {
            return allows(version);
        } else if (other instanceof VersionRange versionRange) {
            return !versionRange.strictlyLower(this) && !versionRange.strictlyHigher(this);
        } else if (other instanceof VersionUnion versionUnion) {
            return versionUnion.ranges().stream().anyMatch(this::allowsAny);
        }
        throw new IllegalArgumentException();
    }

    @Override
    public @NotNull VersionConstraint intersect(VersionConstraint other) {
        if (other.isEmpty()) {
            return other;
        } else if (other instanceof Version version) {
            return allows(version) ? version : VersionConstraint.EMPTY;
        } else if (other instanceof VersionRange versionRange) {
            Version intersectMin;
            boolean intersectIncludeMin;
            if (allowsLower(versionRange)) {
                if (this.strictlyLower(versionRange)) return VersionConstraint.EMPTY;
                intersectMin = versionRange.min();
                intersectIncludeMin = versionRange.includeMin();
            } else {
                if (versionRange.strictlyLower(this)) return VersionConstraint.EMPTY;
                intersectMin = min();
                intersectIncludeMin = includeMin();
            }
            Version intersectMax;
            boolean intersectIncludeMax;
            if (allowsHigher(versionRange)) {
                intersectMax = versionRange.max();
                intersectIncludeMax = versionRange.includeMax();
            } else {
                intersectMax = max();
                intersectIncludeMax = includeMax();
            }
            if (intersectMin == null && intersectMax == null) return VersionConstraint.ANY;
            if (Objects.equals(intersectMin, intersectMax)) {
                if (!intersectIncludeMin || !intersectIncludeMax)
                    throw new RuntimeException("single-version intersection not inclusive");

                return intersectMin;
            }
            return new VersionRange(intersectMin, intersectMax, intersectIncludeMin, intersectIncludeMax);
        } else if (other instanceof VersionUnion versionUnion) {
            return versionUnion.intersect(this);
        }
        throw new IllegalArgumentException();
    }

    @Override
    public @NotNull VersionConstraint union(VersionConstraint other) {
        if (other instanceof Version version) {
            if (allows(version)) return this;
            if (version.equals(min())) return new VersionRange(min(), max(), true, includeMax());
            if (version.equals(max())) return new VersionRange(min(), max(), includeMin(), true);
        } else if (other instanceof VersionRange versionRange) {
            var edgesTouch =
                (max() != null && max().equals(versionRange.min()) && (includeMax() || versionRange.includeMin())) ||
                    (min() != null && min().equals(versionRange.max) && (includeMin() || versionRange.includeMax()));

            if (edgesTouch || allowsAny(other)) {
                Version unionMin;
                boolean unionIncludeMin;

                if (allowsLower(versionRange)) {
                    unionMin = min();
                    unionIncludeMin = includeMin();
                } else {
                    unionMin = versionRange.min();
                    unionIncludeMin = versionRange.includeMin();
                }

                Version unionMax;
                boolean unionIncludeMax;

                if (allowsHigher(versionRange)) {
                    unionMax = max();
                    unionIncludeMax = includeMax();
                } else {
                    unionMax = versionRange.max();
                    unionIncludeMax = versionRange.includeMax();
                }

                return new VersionRange(unionMin, unionMax, unionIncludeMin, unionIncludeMax);
            }
        }

        return VersionConstraint.unionOf(Stream.of(this, other));
    }

    @Override
    public @NotNull VersionConstraint difference(VersionConstraint other) {
        if (other.isEmpty()) {
            return this;
        } else if (other instanceof Version version) {
            if (!allows(version)) return this;
            if (version.equals(min())) {
                if (!includeMin()) return this;
                return new VersionRange(min(), max(), false, includeMax());
            }
            if (version.equals(max())) {
                if (!includeMax()) return this;
                return new VersionRange(min(), max(), includeMin(), false);
            }
            return new VersionUnion(List.of(new VersionRange(min(), version, includeMin(), false),
                                            new VersionRange(version, max(), false, includeMax())));
        } else if (other instanceof VersionRange versionRange) {
            if (!allowsAny(versionRange)) return this;
            VersionRange before;
            if (!allowsLower(versionRange)) {
                before = null;
            } else if (min().equals(versionRange.min())) {
                before = min();
            } else {
                before = new VersionRange(min(), versionRange.min(), includeMin(), !versionRange.includeMin());
            }
            VersionRange after;
            if (!allowsHigher(versionRange)) {
                after = null;
            } else if (max().equals(versionRange.max())) {
                after = max();
            } else {
                after = new VersionRange(versionRange.max(), max(), !versionRange.includeMax(), includeMax());
            }
            if (before == null && after == null) return VersionConstraint.EMPTY;
            if (before == null) return after;
            if (after == null) return before;
            return new VersionUnion(List.of(before, after));
        } else if (other instanceof VersionUnion versionUnion) {
            var ranges = new ArrayList<VersionRange>();
            var current = this;
            for (var range : versionUnion.ranges()) {
                if (range.strictlyLower(current)) continue;
                if (range.strictlyHigher(current)) break;

                var difference = current.difference(range);
                if (difference.isEmpty()) {
                    return VersionConstraint.EMPTY;
                } else if (difference instanceof VersionRange versionRange) {
                    current = versionRange;
                } else if (difference instanceof VersionUnion union) {
                    if (union.ranges().size() != 2) throw new RuntimeException("impossible difference result");
                    ranges.add(union.ranges().get(0));
                    current = union.ranges().get(1);
                } else {
                    throw new IllegalArgumentException();
                }
            }
            if (ranges.isEmpty()) return current;
            ranges.add(current);
            return new VersionUnion(ranges);
        }
        throw new IllegalArgumentException();
    }

    @Override
    public int compareTo(VersionRange other) {
        if (min() == null) {
            if (other.min() == null) return compareMax(other);
            return -1;
        } else if (other.min() == null) {
            return 1;
        }

        var result = min().compareTo(other.min());
        if (result != 0) return result;
        if (includeMin() != other.includeMin()) return includeMin() ? -1 : 1;

        return compareMax(other);
    }

    private int compareMax(VersionRange other) {
        if (max() == null) {
            if (other.max() == null) return 0;
            return 1;
        } else if (other.max() == null) {
            return -1;
        }

        var result = max().compareTo(other.max());
        if (result != 0) return result;
        if (includeMax() != other.includeMax()) return includeMax() ? 1 : -1;

        return 0;
    }

    /**
     * Returns whether this range is immediately next to, but not overlapping, {@code other}.
     */
    public boolean areAdjacent(VersionRange other) {
        if (max() != other.min()) return false;

        return (includeMax() && !other.includeMin()) || (!includeMax() && other.includeMin());
    }

    /**
     * Returns whether this range allows lower versions than {@code other}.
     */
    public boolean allowsLower(VersionRange other) {
        if (min() == null) return other.min() != null;
        if (other.min() == null) return false;

        var comparison = min().compareTo(other.min());
        if (comparison < 0) return true;
        if (comparison > 0) return false;
        return includeMin() && !other.includeMin();
    }

    /**
     * Returns whether this range allows higher versions than {@code other}.
     */
    public boolean allowsHigher(VersionRange other) {
        if (max() == null) return other.max() != null;
        if (other.max() == null) return false;

        var comparison = max().compareTo(other.max());
        if (comparison > 0) return true;
        if (comparison < 0) return false;
        return includeMax() && !other.includeMax();
    }

    /**
     * Returns whether this range allows only versions lower than those allowed by {@code other}.
     */
    public boolean strictlyLower(VersionRange other) {
        if (max() == null || other.min() == null) return false;

        var comparison = max().compareTo(other.min());
        if (comparison < 0) return true;
        if (comparison > 0) return false;
        return !includeMax() || !other.includeMin();
    }

    /**
     * Returns whether this range allows only versions higher than those allowed by {@code other}.
     */
    public boolean strictlyHigher(VersionRange other) {
        return other.strictlyLower(this);
    }

    public Version min() {
        return min;
    }

    public Version max() {
        return max;
    }

    public boolean includeMin() {
        return includeMin;
    }

    public boolean includeMax() {
        return includeMax;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (VersionRange) obj;
        return Objects.equals(this.min, that.min) && Objects.equals(this.max, that.max) &&
            this.includeMin == that.includeMin && this.includeMax == that.includeMax;
    }

    @Override
    public int hashCode() {
        return Objects.hash(min, max, includeMin, includeMax);
    }

    @Override
    public String toString() {
        var buffer = new StringBuilder();

        if (min() != null) {
            buffer.append(includeMin() ? ">=" : ">");
            buffer.append(min());
        }

        if (max() != null) {
            if (min() != null) buffer.append(' ');
            if (includeMax()) {
                buffer.append("<=");
            }
            buffer.append(max());
        }

        if (min() == null && max() == null) buffer.append("any");
        return buffer.toString();
    }

}
