package dev.vriska.jpubgrub;

/**
 * The incompatibility represents a package's dependency.
 */
public class DependencyIncompatibilityCause extends IncompatibilityCause {
    public final PackageRange depender;
    public final PackageRange target;

    public DependencyIncompatibilityCause(PackageRange depender, PackageRange target) {
        this.depender = depender;
        this.target = target;
    }
}
