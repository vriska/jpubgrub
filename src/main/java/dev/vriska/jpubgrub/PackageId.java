package dev.vriska.jpubgrub;

import dev.vriska.jpubgrub.versions.Version;

public record PackageId(String name, Version version, boolean isRoot) {
    public PackageRange toRange() {
        return new PackageRange(toRef(), version);
    }

    public PackageRef toRef() {
        return new PackageRef(name, isRoot);
    }
}
