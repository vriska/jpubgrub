package dev.vriska.jpubgrub;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A set of mutually-incompatible terms.
 * <p>
 * See <a href="https://github.com/dart-lang/pub/tree/master/doc/solver.md#incompatibility">solver.md</a>.
 */
public class Incompatibility {
    /**
     * The mutually-incompatible terms.
     */
    public final List<Term> terms;

    /**
     * The reason {@code terms} are incompatible.
     */
    public final IncompatibilityCause cause;

    /**
     * Whether this incompatibility indicates that version solving as a whole has failed.
     */
    public boolean isFailure() {
        return terms.isEmpty() || (terms.size() == 1 && terms.get(0).packageRange().isRoot());
    }

    /**
     * Returns all external incompatibilities in this incompatibility's derivation graph.
     */
    Stream<Incompatibility> externalIncompatibilities() {
        if (cause instanceof ConflictCause conflictCause) {
            return Stream.concat(conflictCause.conflict.externalIncompatibilities(),
                                 conflictCause.other.externalIncompatibilities());
        } else {
            return Stream.of(this);
        }
    }

    /**
     * Creates an incompatibility with {@code terms}.
     * <p>
     * This normalizes {@code terms} so that each package has at most one term referring to it.
     */
    public Incompatibility(List<Term> terms, IncompatibilityCause cause) {
        this.cause = cause;

        if (terms.size() == 1 && cause instanceof ConflictCause &&
            terms.stream().anyMatch(term -> term.isPositive() && term.packageRange().isRoot())) {
            terms = terms.stream().filter(term -> !term.isPositive() || !term.packageRange().isRoot()).toList();
        }

        if (terms.size() == 1 ||
            (terms.size() == 2 && !terms.get(0).packageRange().name().equals(terms.get(1).packageRange().name()))) {
            this.terms = terms;
            return;
        }

        var byName = new HashMap<String, Map<PackageRef, Term>>();
        for (var term : terms) {
            var byRef = byName.computeIfAbsent(term.packageRange().name(), k -> new HashMap<>());
            var ref = term.packageRange().ref();
            byRef.merge(ref, term, Term::intersect);
        }

        this.terms = byName.values().stream().flatMap(byRef -> {
            var positiveTerms = byRef.values().stream().filter(Term::isPositive).toList();
            if (!positiveTerms.isEmpty()) {
                return positiveTerms.stream();
            } else {
                return byRef.values().stream();
            }
        }).toList();
    }

    @Override
    public String toString() {
        if (cause instanceof DependencyIncompatibilityCause) {
            if (terms.size() != 2) throw new IllegalArgumentException(
                "DependencyIncompatibilityCause with wrong number of terms (" + terms.size() + ")");

            var depender = terms.get(0);
            var dependee = terms.get(1);

            if (!depender.isPositive())
                throw new IllegalArgumentException("DependencyIncompatibilityCause with negative depender");
            if (dependee.isPositive())
                throw new IllegalArgumentException("DependencyIncompatibilityCause with positive dependee");

            return terse(depender, true) + " depends on " + terse(dependee);
        } else if (cause instanceof NoVersionsIncompatibilityCause) {
            if (terms.size() != 1) throw new IllegalArgumentException(
                "NoVersionsIncompatibilityCause with wrong number of terms (" + terms.size() + ")");
            if (!terms.get(0).isPositive())
                throw new IllegalArgumentException("NoVersionsIncompatibilityCause with negative term");
            return "no versions of " + terseRef(terms.get(0)) + " match " + terms.get(0).constraint();
        } else if (cause instanceof RootIncompatibilityCause) {
            if (terms.size() != 1) throw new IllegalArgumentException(
                "RootIncompatibilityCause with wrong number of terms (" + terms.size() + ")");
            if (terms.get(0).isPositive())
                throw new IllegalArgumentException("RootIncompatibilityCause with positive term");
            if (!terms.get(0).packageRange().isRoot())
                throw new IllegalArgumentException("RootIncompatibilityCause with non-root package");
            return terms.get(0).packageRange().name() + " is " + terms.get(0).constraint();
        } else if (isFailure()) {
            return "version solving failed";
        }

        if (terms.size() == 1) {
            var term = terms.get(0);
            if (term.constraint().isAny()) {
                return terseRef(term) + " is " + (term.isPositive() ? "forbidden" : "required");
            } else {
                return terse(term) + " is " + (term.isPositive() ? "forbidden" : "required");
            }
        }

        if (terms.size() == 2) {
            var term1 = terms.get(0);
            var term2 = terms.get(1);
            if (term1.isPositive() == term2.isPositive()) {
                if (term1.isPositive()) {
                    var package1 = term1.constraint().isAny() ? terseRef(term1) : terse(term1);
                    var package2 = term2.constraint().isAny() ? terseRef(term2) : terse(term2);
                    return package1 + " is incompatible with " + package2;
                } else {
                    return "either " + terse(term1) + " or " + terse(term2);
                }
            }
        }

        var positive = new ArrayList<String>();
        Term positiveTerm = null;
        var negative = new ArrayList<String>();

        for (var term : terms) {
            if (term.isPositive()) {
                positive.add(terse(term));
                positiveTerm = term;
            } else {
                negative.add(terse(term));
            }
        }

        if (!positive.isEmpty() && !negative.isEmpty()) {
            if (positive.size() == 1) {
                return terse(positiveTerm, true) + " requires " + String.join(" or ", negative);
            } else {
                return "if " + String.join(" or ", positive) + " then " + String.join(" or ", negative);
            }
        } else if (!positive.isEmpty()) {
            return "one of " + String.join(" or ", positive) + " must be false";
        } else {
            return "one of " + String.join(" or ", negative) + " must be true";
        }
    }

    /**
     * Returns the equivalent of {@code "$this and $other"}, with more intelligent phrasing for specific patterns.
     * <p>
     * If {@code thisLine} and/or {@code otherLine} are passed, they indicate line numbers that should be associated
     * with {@code this} and {@code other} respectively.
     */
    public String andToString(Incompatibility other, Integer thisLine, Integer otherLine) {
        var requiresBoth = tryRequiresBoth(other, thisLine, otherLine);
        if (requiresBoth != null) return requiresBoth;

        var requiresThrough = tryRequiresThrough(other, thisLine, otherLine);
        if (requiresThrough != null) return requiresThrough;

        var requiresForbidden = tryRequiresForbidden(other, thisLine, otherLine);
        if (requiresForbidden != null) return requiresForbidden;

        var buffer = new StringBuilder(toString());
        if (thisLine != null) buffer.append(' ').append(thisLine);
        buffer.append(" and ");
        buffer.append(other.toString());
        if (otherLine != null) buffer.append(' ').append(otherLine);
        return buffer.toString();
    }

    /**
     * If "{@code this} and {@code other}" can be expressed as "some package requires both X and Y", this returns that
     * expression.
     * <p>
     * Otherwise, this returns null.
     */
    private String tryRequiresBoth(Incompatibility other, Integer thisLine, Integer otherLine) {
        if (terms.size() == 1 || other.terms.size() == 1) return null;

        var thisPositive = singleTermWhere(Term::isPositive);
        if (thisPositive == null) return null;
        var otherPositive = other.singleTermWhere(Term::isPositive);
        if (otherPositive == null) return null;
        if (!thisPositive.packageRange().equals(otherPositive.packageRange())) return null;

        var thisNegatives = terms.stream()
            .filter(term -> !term.isPositive())
            .map(Incompatibility::terse)
            .collect(Collectors.joining(" or "));
        var otherNegatives = other.terms.stream()
            .filter(term -> !term.isPositive())
            .map(Incompatibility::terse)
            .collect(Collectors.joining(" or "));

        StringBuilder buffer = new StringBuilder(terse(thisPositive, true));
        var isDependency =
            cause instanceof DependencyIncompatibilityCause && other.cause instanceof DependencyIncompatibilityCause;
        buffer.append(isDependency ? " depends on both " : " requires both ");
        buffer.append(thisNegatives);
        if (thisLine != null) buffer.append(" (").append(thisLine).append(")");
        buffer.append(" and ").append(otherNegatives);
        if (otherLine != null) buffer.append(" (").append(otherLine).append(")");
        return buffer.toString();
    }

    /**
     * If "{@code this} and {@code other}" can be expressed as "X requires Y which requires Z", this returns that
     * expression.
     * <p>
     * Otherwise, this returns null.
     */
    private String tryRequiresThrough(Incompatibility other, Integer thisLine, Integer otherLine) {
        if (terms.size() == 1 || other.terms.size() == 1) return null;

        var thisNegative = singleTermWhere(term -> !term.isPositive());
        var otherNegative = other.singleTermWhere(term -> !term.isPositive());
        if (thisNegative == null && otherNegative == null) return null;

        var thisPositive = singleTermWhere(Term::isPositive);
        var otherPositive = singleTermWhere(Term::isPositive);

        Incompatibility prior;
        Term priorNegative;
        Integer priorLine;
        Incompatibility latter;
        Integer latterLine;
        if (thisNegative != null && otherPositive != null &&
            thisNegative.packageRange().name().equals(otherPositive.packageRange().name()) &&
            thisNegative.inverse().satisfies(otherPositive)) {
            prior = this;
            priorNegative = thisNegative;
            priorLine = thisLine;
            latter = other;
            latterLine = otherLine;
        } else if (otherNegative != null && thisPositive != null &&
            otherNegative.packageRange().name().equals(thisPositive.packageRange().name()) &&
            otherNegative.inverse().satisfies(thisPositive)) {
            prior = other;
            priorNegative = otherNegative;
            priorLine = otherLine;
            latter = this;
            latterLine = thisLine;
        } else {
            return null;
        }

        var priorPositives = prior.terms.stream().filter(Term::isPositive).toList();

        var buffer = new StringBuilder();
        if (priorPositives.size() > 1) {
            var priorString = priorPositives.stream().map(Incompatibility::terse).collect(Collectors.joining(" or "));
            buffer.append("if ").append(priorString).append(" then ");
        } else {
            buffer.append(terse(priorPositives.get(0), true));
            buffer.append(prior.cause instanceof DependencyIncompatibilityCause ? " depends on " : " requires ");
        }

        buffer.append(terse(priorNegative));
        if (priorLine != null) buffer.append(" (").append(priorLine).append(")");
        buffer.append(" which ");

        buffer.append(latter.cause instanceof DependencyIncompatibilityCause ? "depends on " : "requires ");

        buffer.append(latter.terms.stream()
                          .filter(term -> !term.isPositive())
                          .map(Incompatibility::terse)
                          .collect(Collectors.joining(" or ")));

        if (latterLine != null) buffer.append(" (").append(latterLine).append(")");

        return buffer.toString();
    }

    /**
     * If "{@code this} and {@code other}" can be expressed as "X requires Y which is forbidden", this returns that
     * expression.
     * <p>
     * Otherwise, this returns null.
     */
    private String tryRequiresForbidden(Incompatibility other, Integer thisLine, Integer otherLine) {
        if (terms.size() != 1 && other.terms.size() != 1) return null;

        Incompatibility prior;
        Incompatibility latter;
        Integer priorLine;
        Integer latterLine;
        if (terms.size() == 1) {
            prior = other;
            latter = this;
            priorLine = otherLine;
            latterLine = thisLine;
        } else {
            prior = this;
            latter = other;
            priorLine = thisLine;
            latterLine = otherLine;
        }

        var negative = prior.singleTermWhere(term -> !term.isPositive());
        if (negative == null) return null;
        if (!negative.inverse().satisfies(latter.terms.get(0))) return null;

        var positives = prior.terms.stream().filter(Term::isPositive).toList();

        var buffer = new StringBuilder();
        if (positives.size() > 1) {
            var priorString = positives.stream().map(Incompatibility::terse).collect(Collectors.joining(" or "));
            buffer.append("if ").append(priorString).append(" then ");
        } else {
            buffer.append(terse(positives.get(0), true));
            buffer.append(prior.cause instanceof DependencyIncompatibilityCause ? " depends on " : " requires ");
        }

        buffer.append(terse(latter.terms.get(0)));
        if (priorLine != null) buffer.append(" (").append(priorLine).append(")");

        if (latter.cause instanceof NoVersionsIncompatibilityCause) {
            buffer.append(" which doesn't match any versions");
        } else {
            buffer.append(" which is forbidden");
        }

        if (latterLine != null) buffer.append(" (").append(latterLine).append(")");

        return buffer.toString();
    }

    /**
     * If exactly one term in this incompatibility matches {@code filter}, returns that term.
     * <p>
     * Otherwise, returns null.
     */
    private Term singleTermWhere(Predicate<Term> filter) {
        Term found = null;
        for (var term : terms) {
            if (!filter.test(term)) continue;
            if (found != null) return null;
            found = term;
        }
        return found;
    }

    /**
     * Returns a terse representation of {@code term}'s package ref.
     */
    private static String terseRef(Term term) {
        return term.packageRange().ref().toString();
    }

    /**
     * Returns a terse representation of {@code term}'s package.
     * <p>
     * If {@code allowEvery} is `true`, this will return "every version of foo" instead of "foo any".
     */
    private static String terse(Term term, boolean allowEvery) {
        if (allowEvery && term.constraint().isAny()) {
            return "every version of " + terseRef(term);
        } else {
            return term.packageRange().toString();
        }
    }

    /**
     * Returns a terse representation of {@code term}'s package.
     */
    private static String terse(Term term) {
        return terse(term, false);
    }
}
