package dev.vriska.jpubgrub;

import dev.vriska.jpubgrub.versions.VersionConstraint;

public record PackageRange(PackageRef ref, VersionConstraint constraint) {
    public String name() {
        return ref.name();
    }

    public boolean isRoot() {
        return ref.isRoot();
    }
}
