package dev.vriska.jpubgrub;

/**
 * The incompatibility was derived from two existing incompatibilities during conflict resolution.
 */
public class ConflictCause extends IncompatibilityCause {
    /**
     * The incompatibility that was originally found to be in conflict, from which the target incompatibility was
     * derived.
     */
    public final Incompatibility conflict;

    /**
     * The incompatibility that caused the most recent satisfier for {@code conflict}, from which the target
     * incompatibility was derived.
     */
    public final Incompatibility other;

    public ConflictCause(Incompatibility conflict, Incompatibility other) {
        this.conflict = conflict;
        this.other = other;
    }
}
